using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.XR.Interaction.Toolkit;


public class SelectEvent : MonoBehaviour
{
    private XRBaseInteractor m_interactor;

    private void Awake()
    {
        m_interactor = this.GetComponent<XRBaseInteractor>();
    }

    void Start()
    {
        // XRBaseInteractor のセレクト時イベントを追加
        m_interactor.selectEntered.AddListener(OnSelectEntered);
        m_interactor.selectExited.AddListener(OnSelectExited);
    }
    void OnSelectEntered(SelectEnterEventArgs args)
    {
        Debug.Log("Enter!");
    }

    void OnSelectExited(SelectExitEventArgs args)
    {
        Debug.Log("Exit!");
    }

}
