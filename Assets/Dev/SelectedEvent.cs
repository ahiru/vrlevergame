using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.XR.Interaction.Toolkit;


public class SelectedEvent : MonoBehaviour
{
    private XRBaseInteractable m_interactable;

    private void Awake()
    {
        m_interactable = this.GetComponent<XRBaseInteractable>();
    }

    void Start()
    {
        // XRBaseInteractor のセレクト時イベントを追加
        m_interactable.selectEntered.AddListener(OnSelectedEntered);
        m_interactable.selectExited.AddListener(OnSelectedExited);
    }
    public void OnSelectedEntered(SelectEnterEventArgs args)
    {
        Debug.Log("Enter!");
    }

    void OnSelectedExited(SelectExitEventArgs args)
    {
        Debug.Log("Exit!");
    }

}
