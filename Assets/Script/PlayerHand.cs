﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.XR.Interaction.Toolkit;

public class PlayerHand : MonoBehaviour
{
    private XRBaseInteractor m_interactor;
    private XRBaseInteractable m_grabbingObject;

    private void Awake()
    {
        m_interactor = this.GetComponent<XRBaseInteractor>();
    }
    // Start is called before the first frame update
    void Start()
    {
        // XRBaseInteractor のセレクト時イベントを追加
        m_interactor.selectEntered.AddListener(OnSelectEntered);
        m_interactor.selectExited.AddListener(OnSelectExited);
    }

    void OnSelectEntered(SelectEnterEventArgs args)
    {
        m_grabbingObject = args.interactable;
        var interfacePositionSettable = m_grabbingObject as IPositionSettable;
        interfacePositionSettable.InitialPosition = this.transform.position;
    }
    void OnSelectExited(SelectExitEventArgs args) { m_grabbingObject = null; }

    // Update is called once per frame
    void Update()
    {
        if(m_grabbingObject != null)
        {
            // XRGrabInteractable 継承のばあいは, 直接キャストしていい
            var interfacePositionSettable = m_grabbingObject as IPositionSettable;

            // IPositionSettable インタフェースを実装した つかめるものである
            if (interfacePositionSettable != null)
            {
                // 今の手の座標をわたす
                interfacePositionSettable.SetTargetPosition(this.transform.position);

                // 今の手の座標を強制的に更新する処理とかをやってもいい
                // 掴んでいるものに吸い付かせるとか
                // m_currentHandPos = destPositon;
            }
        }
    }
}
