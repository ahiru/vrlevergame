﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

class RotatableLever : LeverBase
{
    private Quaternion from;
    private Quaternion to;
    [SerializeField]
    private float leverHeight;
    private float thresholdRatio = 0.2f;
    [SerializeField]
    private GameObject blueLever;

    protected override float CalculateDestPosition(Vector3 targetPosition)
    {
        var difY = targetPosition.y - m_initialPosition.y;
        if(difY != 0.0f)
        {
            range -= (difY / leverHeight);

            var blever = blueLever.GetComponent<LeverBase>();
            float blueRange = blever.Range;

            if (blueRange > 0.45f)
            {
                range = Mathf.Clamp(range, 0f, 0.65f);
            }
            else
            {
                range = Mathf.Clamp01(range);
            }
            
            transform.rotation = Quaternion.Slerp(from, to, range);
            m_initialPosition.y = targetPosition.y;
        }

        return range; 
    }

    
    protected override float CalculateSignal(float range)  // [0,1]の範囲であることを保証する
    {
        float signal = fit(range,thresholdRatio);
        signal = Mathf.Clamp01(signal);

        // Debug.Log("range = " + signal.ToString());

        return signal; 
    }

    private void Start()
    {
        from = Quaternion.AngleAxis(60, Vector3.right);
        to = Quaternion.AngleAxis(-55, Vector3.right);
        range = 0.0f;
        transform.rotation = Quaternion.Slerp(from, to, range);
    }

    float fit(float x, float thresholdRatio)
    {
        float y;
        float min = thresholdRatio;
        float max = 1.0f - thresholdRatio;
        x = Mathf.Clamp(x, min, max);
        y = ((x - min) / (max - min));
        return y;
    }
}