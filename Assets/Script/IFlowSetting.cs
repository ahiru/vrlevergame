using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

interface IFlowSetting
{
    void BindChangedVelocity(Action<Vector3> callback);
    void SetVelocity(Vector3 targetVelocity);
    Vector3 GetVelocity { get; }
}
