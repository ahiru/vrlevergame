using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using UnityEngine.XR.Interaction.Toolkit;


public class Restart : MonoBehaviour
{
    private event Action m_restart;

    public void BindRestart(Action callback) { m_restart += callback; }


   public void RestartGame() 
    {
        GameObject[] matters = GameObject.FindGameObjectsWithTag("Matter");
        foreach(GameObject matter in matters)
        {
            Destroy(matter);
        }
        m_restart?.Invoke();
    }
}
