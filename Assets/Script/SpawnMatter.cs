﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnMatter : MonoBehaviour
{
    // InspectorでMatterのPrefabをアタッチしておく
    [SerializeField] GameObject normal;
    [SerializeField] GameObject deblis;
    [SerializeField] GameObject foreign;

    [SerializeField] GameObject conveyor;
    [SerializeField]
    GameObject restartObj;


    private bool gamestart = false ;
    
    // 重なりが離れたらスポーンする
    private void OnTriggerExit(Collider other)
    {
        if (gamestart)
        {
            GameObject obj = MatterRamdomizer();
            SponeMatter(obj);
        }
    }

    // スポーンするオブジェクトをランダムに決定する
    private GameObject MatterRamdomizer()
    {
        // Matter.Typeの項目数を取得 (InspectorにアタッチされているMatterPrefabの数と一致する前提がある)
        int typecount = System.Enum.GetNames(typeof(Matter.Type)).Length;
        int i = Random.Range(0,typecount);
        Matter.Type type = (Matter.Type)i;

        GameObject obj = null;

        if (conveyor != null)
        {
            var conveyorComp = conveyor.GetComponent<IFlowSetting>();
            switch (type)
            {
                case Matter.Type.Normal:
                    obj = normal;
                    break;
                case Matter.Type.Deblis:
                    obj = deblis;
                    break;
                case Matter.Type.Foreign:
                    obj = foreign;
                    break;
            }
            var matter = obj.GetComponent<Matter>();
            matter.Conveyor = conveyor;
            matter.Velocity = conveyorComp.GetVelocity;
        }
        return obj;
    }


    private void SponeMatter(GameObject obj)
    {
        // スポーン処理
        Instantiate(obj, this.transform.position, Quaternion.identity);
    }

    private void Start()
    {
        var restart = restartObj.GetComponent<Restart>();
        restart.BindRestart(Restart);
    }

    public void Restart()
    {
        gamestart = true;
        GameObject obj = MatterRamdomizer();
        SponeMatter(obj);
    }
}
