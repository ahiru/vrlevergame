﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

interface IPositionSettable
{
    // レバーの目的位置を設定できる
    Vector3 SetTargetPosition(Vector3 targetPosition);

    Vector3 InitialPosition { get; set; }

    // レバーの現在位置を取得できる
    Vector3 GetPosition { get; }
}
