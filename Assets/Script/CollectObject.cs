using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CollectObject : MonoBehaviour
{
    [SerializeField]
    GameObject scoreObj;

    private void OnTriggerEnter(Collider other)
    {
        var matter = other.gameObject.GetComponent<Matter>();
        Matter.Type type = matter.Gettype;

        var score = scoreObj.GetComponent<ScoreManager>();

        switch(type)
        {
            case Matter.Type.Normal:
                score.addScore();
                break;
            case Matter.Type.Deblis:
                score.minusScore();
                break;
            case Matter.Type.Foreign:
                score.minus2Score();
                break;
        }
        Destroy(other.gameObject.transform.root.gameObject);
    }
}
