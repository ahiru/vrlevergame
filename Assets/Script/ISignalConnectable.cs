﻿using System;
using System.Collections;
using System.Collections.Generic;

// [0, 1]の間の信号がもらえるインタフェース
interface ISignalConnectable
{
    // [0,1]の信号が変化したら呼ばれるコールバック関数を登録することができる
    void BindChangedValue(Action<float> callback);
    // いまの信号値ももらえる
    float SignalValue { get; }
}