﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Matter : MonoBehaviour
{
    private int m_scorePoint;   // スコア計算で使う
    //[SerializeField]
    //private GameObject sponer;
    [SerializeField]
    private Vector3 m_velocity;
    [SerializeField]
    private GameObject conveyor;

    public enum Type { Normal, Deblis, Foreign }
    [SerializeField]
    private Type type;

    public Type Gettype => type;
    public GameObject Conveyor
    {
        set { conveyor = value; }
        get { return conveyor; }
    }
 
    public Vector3 Velocity
    {
        set { m_velocity = value; }
        get { return m_velocity; }
    }

    private void Awake()
    {

    }
    void Start()
    {

    }

    private void OnEnable()
    {
        if (conveyor != null)
        {
            var interfaceFlowSetting = conveyor.GetComponent<IFlowSetting>();
            interfaceFlowSetting.BindChangedVelocity(UpdateVelocity);
        }
    }

    void Update()
    {
        // m_velocityで自らを移動させる処理
        transform.position += m_velocity * Time.deltaTime;
    }

    void UpdateVelocity(Vector3 conveyorVelocity)
    {
        m_velocity = conveyorVelocity;
        
    }

    public void Clash()
    {
        switch (type)
        {
            case Matter.Type.Normal:
                Destroy(this.gameObject.transform.root.gameObject);
                break;
            case Matter.Type.Deblis:
                break;
            case Matter.Type.Foreign:
                GameObject obj = (GameObject)Resources.Load("Deblis");
                Vector3 pos = this.transform.root.position;
                var deblis = obj.GetComponent<Matter>();
                deblis.Conveyor = this.Conveyor;

                Destroy(this.gameObject.transform.root.gameObject);
                Instantiate(obj, pos, Quaternion.identity);

                break;
        }

    }

}
