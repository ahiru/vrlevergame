using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class Conveyor : MonoBehaviour, IFlowSetting
{
    private Vector3 m_velocity;
    [SerializeField]
    private Vector3 m_initialvelocity;
    private event Action<Vector3> m_changedVelocity;
    [SerializeField]
    private GameObject Rlever;
    [SerializeField]
    private GameObject Blever;
    float m_rSignal = 0f;
    float m_bSignal = 0f;

    public void BindChangedVelocity(Action<Vector3> callback) { m_changedVelocity += callback; }
    
    public void SetVelocity(Vector3 targetVelocity)
    {
        m_velocity = targetVelocity;
        m_changedVelocity?.Invoke(m_velocity);
    }

    public Vector3 GetVelocity => m_velocity;

    private void Awake()
    {
        m_velocity = m_initialvelocity;
    }

    void Start()
    {
        if(Rlever != null)
        {
            var rl = Rlever.GetComponent<ISignalConnectable>();
            rl.BindChangedValue(OnRSignalChanged);
        }
        if (Blever != null)
        {
            var bl = Blever.GetComponent<ISignalConnectable>();
            bl.BindChangedValue(OnBSignalChanged);
        }

    }

    void Update()
    {
    }


    void OnRSignalChanged(float m_signal)
    {
        m_rSignal = m_signal;
        // Debug.Log(m_rSignal.ToString());
        if(m_rSignal > 0.01f || m_bSignal > 0.01f)
        {
            SetVelocity(new Vector3(0, 0, 0));
        }else
        {
            SetVelocity(m_initialvelocity);
        }
    }

    void OnBSignalChanged(float m_signal)
    {
        m_bSignal = m_signal;
        if (m_rSignal > 0.01f || m_bSignal > 0.01f)
        {
            SetVelocity(new Vector3(0, 0, 0));
        }
        else
        {
            SetVelocity(m_initialvelocity);
        }

    }

}
