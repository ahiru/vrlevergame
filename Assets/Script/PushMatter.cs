using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PushMatter : MonoBehaviour
{
    private Vector3 matterPosition;

    private void OnTriggerEnter(Collider other)
    {
        matterPosition = other.transform.root.position;
    }

    private void OnTriggerStay(Collider other)
    {
        Vector3 pos = other.transform.root.position;
        float posZ = this.transform.position.z + 0.4f;
        other.transform.root.position = new Vector3(pos.x, pos.y, posZ);
    }
}
