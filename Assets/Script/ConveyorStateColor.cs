using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class ConveyorStateColor : MonoBehaviour
{
    [SerializeField]
    GameObject leverObj;
    [SerializeField]
    Material green;
    [SerializeField]
    Material red;

    private void Start()
    {
        MeshRenderer mr = this.GetComponent<MeshRenderer>();
        mr.sharedMaterial = green;

        var lever = leverObj.GetComponent<ISignalConnectable>();
        lever.BindChangedValue(ChangeColor);
    }

    void ChangeColor(float m_signal)
    {
        if (m_signal < 0.01f)
        {
            MeshRenderer mr = this.GetComponent<MeshRenderer>();
            mr.sharedMaterial = green;
        }else
        {
            MeshRenderer mr = this.GetComponent<MeshRenderer>();
            mr.sharedMaterial = red;

        }
    }
}
