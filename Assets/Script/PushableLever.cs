﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

class PushableLever : LeverBase
{
    private Quaternion from;
    private Quaternion to;
    [SerializeField]
    private float leverRange = 0.2f;
    private float thresholdRatio = 0.2f;
    private float initialZ;
    [SerializeField]
    private GameObject redLever;


    protected override float CalculateDestPosition(Vector3 targetPosition)
    {
        var difZ = targetPosition.z - m_initialPosition.z;
        if (difZ != 0.0f)
        {
            
            Vector3 pos = transform.position;
            float posZ;

            range += (difZ / leverRange);

            var rlever = redLever.GetComponent<LeverBase>();
            float redRange = rlever.Range;

            if (redRange > 0.8f)
            {
                range = Mathf.Clamp(range, 0f, 0.45f);
            }
            else
            {
                range = Mathf.Clamp01(range);
            }
            // Debug.Log(range.ToString());
            posZ = Mathf.Lerp(initialZ, initialZ +leverRange, range);
            pos.z = posZ;
            transform.position = pos;
            m_initialPosition.z = targetPosition.z;
        }

        return range;
    }


    protected override float CalculateSignal(float range)  // [0,1]の範囲であることを保証する
    {
        float signal = fit(range, thresholdRatio);
        signal = Mathf.Clamp01(signal);

        // Debug.Log("range = " + signal.ToString());

        return signal;
    }

    private void Start()
    {
        range = 0.0f;
        initialZ = transform.position.z;
    }

    float fit(float x, float thresholdRatio)
    {
        float y;
        float min = thresholdRatio;
        float max = 1.0f - thresholdRatio;
        x = Mathf.Clamp(x, min, max);
        y = ((x - min) / (max - min));
        return y;
    }
}
