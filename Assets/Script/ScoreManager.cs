﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScoreManager : MonoBehaviour
{
    private int m_score = 0;
    private float time = 60.0f;
    private bool gamestart = false;
    [SerializeField]
    GameObject scoreText;
    [SerializeField]
    GameObject timeText;
    [SerializeField]
    GameObject restartObj;

    public float GetTime => time;

    public void addScore()
    {
        if (time > 0f) m_score++;
    }
    
    public void minus2Score()
    {
        if (time > 0f) m_score -= 2;
    }
  
    public void minusScore()
    {
        if(time > 0f) m_score--;
    }

    private void Update()
    {
        if(gamestart)
        {
            time -= Time.deltaTime;
            time = Mathf.Max(0f, time);
        }

        TextMesh score = scoreText.GetComponent<TextMesh>();
        score.text = "Score : " + m_score.ToString();
        TextMesh timeboard = timeText.GetComponent<TextMesh>();
        timeboard.text = "Time : " + time.ToString("f1");

    }

    private void Start()
    {
        var restart = restartObj.GetComponent<Restart>();
        restart.BindRestart(Restart);
    }

    public void Restart()
    {
        time = 60.0f;
        m_score = 0;
        gamestart = true;
    }

}
