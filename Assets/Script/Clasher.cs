using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Clasher : MonoBehaviour
{

    [SerializeField]
    private GameObject obj;
    Vector3 sourcePos;
    public enum Axis
    {
        Y, Z,
    }

    public Axis axis;

    void Start()
    {
        if(obj != null) 
        {
            var lever = obj.GetComponent<ISignalConnectable>();
            switch(axis)
            {
                case Axis.Y:
                    lever.BindChangedValue(MoveOnY);
                    break;
                case Axis.Z:
                    lever.BindChangedValue(MoveOnZ);
                    break;
            }
        }
        sourcePos = transform.position;
    }

    public float Height;
    
    void MoveOnY(float m_signal)
    {
        float targetY = Mathf.Lerp(sourcePos.y, sourcePos.y - Height, m_signal);
        Vector3 curremtPos;
        curremtPos = sourcePos;
        curremtPos.y = targetY;
        transform.position = curremtPos;
    }

    void MoveOnZ(float m_signal)
    {
        float targetZ = Mathf.Lerp(sourcePos.z, sourcePos.z + Height, m_signal);
        Vector3 curremtPos;
        curremtPos = sourcePos;
        curremtPos.z = targetZ;
        transform.position = curremtPos;
    }
}