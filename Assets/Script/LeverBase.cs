﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using UnityEngine.XR.Interaction.Toolkit;

abstract class LeverBase : XRSimpleInteractable, ISignalConnectable, IPositionSettable
{
    #region 全部のレバーで共通の処理はここでやっておく
    protected float m_signal;
    protected Vector3 m_initialPosition;
    protected Vector3 m_currentPosition;
    protected event Action<float> m_changedValue;
    protected float range;
    public float Range { get { return range; } }

    public void BindChangedValue(Action<float> callback) { m_changedValue += callback; }
    public float SignalValue => m_signal;
    public Vector3 InitialPosition
    {
        set { m_initialPosition = value; }
        get { return m_initialPosition; }
    }
    public Vector3 GetPosition => m_currentPosition;
    public Vector3 SetTargetPosition(Vector3 targetPosition)
    {
        range = CalculateDestPosition(targetPosition);
        m_signal = CalculateSignal(range);

        // m_changedValueの引数にm_signalを渡して実行
        m_changedValue?.Invoke(m_signal);
        // 可動域[0,1]の値を返す
        return m_currentPosition;
    }
    #endregion

    #region 各Lever固有の処理をabstractとしておく
    protected abstract float CalculateDestPosition(Vector3 targetPosition);
    protected abstract float CalculateSignal(float range);
    #endregion
}